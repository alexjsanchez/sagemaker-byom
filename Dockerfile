FROM continuumio/miniconda3

RUN apt-get update
RUN apt-get install -y nginx vim
#COPY environment.yml environment.yml

# Use this for install custom packages
#RUN mkdir code
#COPY src code/src
#COPY setup.py code/setup.py
RUN conda update -n base -c defaults conda \
 && conda install -n base python=3.6.8 \
                          anaconda::scikit-learn \
                          anaconda::flask \
                          anaconda::gunicorn \
                          anaconda::gevent \ 
                          conda-forge::pandas \
                          conda-forge::numpy
#RUN conda env create -f environment.yml
#RUN conda run -n myenv pip install ./code/
ENV PYTHONUNBUFFERED=TRUE
ENV PYTHONDONTWRITEBYTECODE=TRUE
ENV PATH="/opt/program:${PATH}"

# Set up the program in the image
COPY program /opt/program
#RUN chmod +x /program/train /program/serve
WORKDIR /opt/program
ENTRYPOINT ["python"]
#ENTRYPOINT ["conda", "run", "-n", "myenv", "python"]

