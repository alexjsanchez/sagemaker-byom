"""Optional code for installing custom package with pip"""

from setuptools import setup, find_packages

setup(
    name='mycode',
    version='0.1.0',
    license='proprietary',
    description='ML Pipeline and Models',

    author='Alex Sanchez',
    author_email='alex.s@slalom.com',
    url='https://sanchez.dev',

    packages=find_packages(where='src'),
    package_dir={'': 'src'}

)
