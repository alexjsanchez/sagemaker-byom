# SageMaker BYOM
This repo holds the generic project structure for trainging and deploying a custom model or algorithm to AWS SageMaker.  


Included is a Jupyter Notebook which can be run in your SageMaker account with a step by step guide to registering your Docker image with ECR, training the model and deploying it via web endpoint or batch transformtion.  


There is also a `local_test` folder so you can test your train and serve scripts on the notebook's EC2 instance before deploying to SageMaker. This is highly recommended due to the difficult and time consuming nature of debugging a deployed model with CloudWatch. For testing the train script locally, your training data needs to be in the `local_test/test_dir/input/data/training/` folder.

## Idiosyncracies
I use conda to manage the Python installation and dependencies. `environment.yml` can be edited to include any additional dependencies from Anaconda Cloud or change Python version (currently set to 3.6.8). You can also pip install dependencies from PyPi using this file as well (see commented out code).


I also like to use pip to install my own local python code via a package--that way I can import my own code into Python without worrying about folder structure. The code should be placed in the `src/mycode/` folder and the `setup.py` file will have the confirguration for the pip installation. The actual installation of the custom package occurs in the Dockerfile and is commented out by default.

## Testing your image
A notebook is provided to help you test your image locally before deploying to ECR. This notebook should be run using notebook instances from the SageMaker console. Be extra careful to test the serving of predicitons because the input data comes in thru a POST request and must be decoded and read by Pandas before being converted to a Pandas DataFrame or other Python object.